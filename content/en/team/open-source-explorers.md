---
title: Explorers
description: Grey Software's Open Source Explorers
category: Team
postion: 13
explorers:  
  - name: Jia Hong
    avatar: https://avatars.githubusercontent.com/u/47465704?s=400&u=9aa7cfe98ec20f7b33abf59e4a7dd1fa92b74b8f
    position: Explorer
    github: https://github.com/jiahongle
    gitlab: https://gitlab.com/jiahongle
    linkedin: https://linkedin.com/in/jiahongle
  - name: Agnes Lin
    avatar: https://avatars3.githubusercontent.com/u/50331796?v=3&s=200
    position: Explorer
    github: https://github.com/agnes512
    gitlab: https://gitlab.com/agnes512
    linkedin: https://www.linkedin.com/in/agnes-yuchi-lin-a4601b172/
  - name: Pranjal Verma
    avatar: https://avatars3.githubusercontent.com/pvcodes
    position: Explorer
    github: https://github.com/pvcodes
    gitlab: https://gitlab.com/pvcodes
    linkedin: https://www.linkedin.com/in/pvcodes
  - name: Kunal Verma
    avatar: https://avatars.githubusercontent.com/u/72245772?s=460&u=756170512bf927ef4410c0d94f3b630510e910cb&v=4
    position: Explorer
    github: https://github.com/Zipher007
    gitlab: https://gitlab.com/Zipher007
    linkedin: https://www.linkedin.com/in/verma-kunal/
  - name: Deepanshu Dhruw
    avatar: https://avatars.githubusercontent.com/u/55559527?s=400&u=7a06beafb54ec1d480eed23bfc3dc91a9a49bd3d
    position: Explorer
    github: https://github.com/devblin
    gitlab: https://gitlab.com/devblin
    linkedin: https://www.linkedin.com/in/devblin/
  - name: Deepak Yadav
    avatar: https://avatars.githubusercontent.com/u/66518459?s=400&u=43bed3001d147bb95a3c958e2ab032e4502f67b7&v=4
    position: Explorer
    github: https://github.com/deepak-yadavdy
    gitlab: https://gitlab.com/deepak_yadavdy
    linkedin: https://www.linkedin.com/in/deepak-yadav-/
  - name: Zaheer Abbas
    avatar: https://secure.gravatar.com/avatar/2b555f12fba4c5d6bd55a96a19463618?s=800&d=identicon
    position: Explorer
    github: https://github.com/nk4456542
    gitlab: https://gitlab.com/nk4456542
    linkedin: https://www.linkedin.com/in/zaheer-abbas-931986175/
  - name: Abhishek Kumar
    avatar: https://avatars.githubusercontent.com/u/48255244?s=460&u=34c3f74b3a931456095c677e1b41c875955b4713&v=4
    position: Explorer
    github: https://github.com/Abhishek-kumar09
    gitlab: https://gitlab.com/Abhishek-kumar09
    linkedin: https://www.linkedin.com/in/abhishek-kumar-016ba1175/
  - name: Sudhanshu Kumar
    avatar: https://avatars.githubusercontent.com/u/63813872?s=400&u=e23bd57ca2f6b8800cc035fb4fbb1f0ea2eed3c0&v=4
    position: Explorer
    github: https://github.com/SudhanshuBlaze
    gitlab: https://gitlab.com/SudhanshuBlaze
    linkedin: https://www.linkedin.com/in/SudhanshuBlaze
---

This page is for the citizens of the internet that wanted to explore the world of open source and did so by completing our [Onboarding Process](https://onboarding.grey.software)

<team-profiles :profiles="explorers"></team-profiles>
